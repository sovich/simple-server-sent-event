<?php

namespace SSE\Tests;

use ArrayIterator;
use IteratorAggregate;
use PHPUnit\Framework\TestCase;
use SSE\Event\Event;
use SSE\Exception\SSEStopException;
use SSE\SSEController;
use Symfony\Component\Uid\Ulid;

final class SSEControllerTest extends TestCase
{
    public function testPingEvent(): void
    {
        $iterator = new EventStoreTest();

        $endTime = time() + 2;
        $handler = new SSEController($iterator, 2, 1, 100);

        $list = [];
        while ($endTime >= time()) {
            foreach ($handler() as $event) {
                $list[] = $event;
            }
            sleep(1);
        }

        self::assertCount(2, $list);
        self::assertContainsEquals('ping', array_map(fn ($event) => $event->getEventname(), $list));
    }

    public function testExitException(): void
    {
        $iterator = new EventStoreTest();

        $endTime = time() + 2;
        $handler = new SSEController($iterator, 1, 100, 100);

        $this->expectException(SSEStopException::class);

        while ($endTime >= time()) {
            foreach ($handler() as $event) {
                $list[] = $event;
            }
            usleep(200);
        }
    }

    public function testEvent(): void
    {
        $event    = new Event(Ulid::generate(), 'test-event');
        $iterator = new EventStoreTest($event);

        $endTime = time() + 2;
        $handler = new SSEController($iterator, 1, 100, 100);

        while ($endTime >= time()) {
            foreach ($handler() as $event) {
                $list[] = $event;
            }
            sleep(1);
        }

        self::assertCount(3, $list);
    }
}

final class EventStoreTest implements IteratorAggregate
{
    private ?Event $event;

    public function __construct(Event $event = null)
    {
        $this->event = $event;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator(null !== $this->event ? [clone $this->event] : []);
    }
}