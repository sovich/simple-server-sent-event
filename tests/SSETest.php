<?php declare(strict_types=1);

namespace SSE\Tests;

use Cache\Adapter\PHPArray\ArrayCachePool;
use PHPUnit\Framework\TestCase;
use SSE\Contract\EventInterface;
use SSE\Event\Event;
use SSE\Repository\EventStoreRepository;
use SSE\SSEEventListenerFactory;

final class SSETest extends TestCase
{
    public function testGoodCase(): void
    {
        $cacheItemPool        = new ArrayCachePool();
        $eventStoreRepository = new EventStoreRepository($cacheItemPool);
        $factory              = new SSEEventListenerFactory($eventStoreRepository);

        $sse = $factory->create(['test-event'], 10, 10, 1);

        $eventStoreRepository->save(new Event('1', 'test-event', 'test1'));
        $eventStoreRepository->save(new Event('1', 'test-event', 'test2'));

        ob_start();
        $sse->start();
        ob_end_flush();
        $raw = ob_get_contents();

        self::assertEquals(<<<RAW
            id: 1
            retry: 5000
            event: test-event
            data: test1
            
            id: 1
            retry: 5000
            event: test-event
            data: test2
            
            
            RAW, $raw);
    }

    public function testIterator(): void
    {
        $cacheItemPool        = new ArrayCachePool();
        $eventStoreRepository = new EventStoreRepository($cacheItemPool);
        $factory              = new SSEEventListenerFactory($eventStoreRepository);

        $sse = $factory->create(['test-event'], 10, 10, 1);

        $eventStoreRepository->save(new Event('1', 'test-event', 'test1'));
        $eventStoreRepository->save(new Event('1', 'test-event', 'test2'));

        /** @var EventInterface[] $events */
        $events = iterator_to_array($sse);

        self::assertCount(2, $events);
        self::assertEquals('test1', $events[0]->getData());
        self::assertEquals('test-event', $events[0]->getEventName());
        self::assertEquals('test2', $events[1]->getData());
        self::assertEquals('test-event', $events[1]->getEventName());
    }

    public function testWithoutScope(): void
    {
        $cacheItemPool        = new ArrayCachePool();
        $eventStoreRepository = new EventStoreRepository($cacheItemPool);
        $factory              = new SSEEventListenerFactory($eventStoreRepository);

        $sse = $factory->create(['buz-event', 'foo-event'], 10, 10, 1);

        $eventStoreRepository->save(new Event('1', 'foo-event', 'test1'));
        $eventStoreRepository->save(new Event('1', 'bar-event', 'test2'));

        /** @var EventInterface[] $events */
        $events = iterator_to_array($sse);

        self::assertCount(1, $events);
        self::assertEquals('test1', $events[0]->getData());
        self::assertEquals('foo-event', $events[0]->getEventName());
    }
}
