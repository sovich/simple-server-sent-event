# Simple Server Sent Event based on cache

### Symfony Controller

```php
use Psr\Cache\CacheItemPoolInterface;
use SSE\Response\SSEResponse;
use SSE\Repository\EventStoreRepository;
use SSE\SSEEventListenerFactory;
use Symfony\Component\Routing\Annotation\Route;

 #[Route(path: '/listener')]
final class TransactionSSEController
{
    public function __invoke(CacheItemPoolInterface $cacheItemPool): SSEResponse
    {
        $eventStoreRepository = new EventStoreRepository($cacheItemPool);
        $factory = new SSEEventListenerFactory($eventStoreRepository);
        $sse = $factory->create(
            events: ['eventName'],
            maxTimeoutWithoutResponse: 60,
            pingInterval: 15,
            maxExecutionTime: 300,
        );

        return new SSEResponse($sse, 500000);
    }
}
```

### Browser
```js
const sse = new EventSource('/listener');
sse.addEventListener('eventName', (event) => {
    console.log(event.data);
});
```

### Push event

```php
use SSE\Event\Event;
use SSE\Repository\EventStoreRepository;
use SSE\SSEEventDispatcher;

$eventStoreRepository = new EventStoreRepository($cacheItemPool);
$event = new Event(uniqid(), 'eventName', 'Patient update');

(new SSEEventDispatcher($eventStoreRepository))->dispatch($event);
```