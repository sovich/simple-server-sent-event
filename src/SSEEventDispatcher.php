<?php declare(strict_types=1);

namespace SSE;

use SSE\Contract\EventInterface;
use SSE\Repository\EventStoreRepository;

final class SSEEventDispatcher
{
    private EventStoreRepository $eventStoreRepository;

    public function __construct(EventStoreRepository $eventStoreRepository)
    {
        $this->eventStoreRepository = $eventStoreRepository;
    }

    public function dispatch(EventInterface $event): void
    {
        $this->eventStoreRepository->save($event);
    }
}
