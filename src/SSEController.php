<?php declare(strict_types=1);

namespace SSE;

use Generator;
use IteratorAggregate;
use SSE\Contract\EventInterface;
use SSE\Event\PingEvent;
use SSE\Exception\SSEStopException;

final class SSEController
{
    /**
     * @var IteratorAggregate<EventInterface>
     */
    private IteratorAggregate $eventStore;

    private int $lastResponse;

    private int $lastPing;

    private int $pingInterval;

    private int $maxTimeoutWithoutResponse;

    private int $maxExecutionTime;

    private int $createTime;

    /**
     * @param IteratorAggregate<EventInterface> $eventStore
     */
    public function __construct(IteratorAggregate $eventStore, int $maxTimeoutWithoutResponse, int $pingInterval, int $maxExecutionTime)
    {
        $this->eventStore                = $eventStore;
        $this->maxTimeoutWithoutResponse = $maxTimeoutWithoutResponse;
        $this->pingInterval              = $pingInterval;
        $this->maxExecutionTime          = $maxExecutionTime;
        $this->createTime                = time();
        $this->lastPing                  = time();
        $this->lastResponse              = time();
    }

    /**
     * @throws SSEStopException
     *
     * @return Generator<EventInterface>
     */
    public function __invoke(): Generator
    {
        $this->stopWatch();
        $pingEvent = $this->generatePingEvent();

        if (null !== $pingEvent) {
            yield $pingEvent;
        }

        foreach ($this->eventStore as $event) {
            $this->lastResponse = time();
            yield $event;
        }
    }

    private function stopWatch(): void
    {
        if (($this->createTime + $this->maxExecutionTime) < time()) {
            throw new SSEStopException();
        }

        if (($this->lastResponse + $this->maxTimeoutWithoutResponse) < time()) {
            throw new SSEStopException();
        }
    }

    private function generatePingEvent(): ?PingEvent
    {
        if ($this->lastPing <= (time() - $this->pingInterval)) {
            $this->lastPing = time();

            return new PingEvent(uniqid());
        }

        return null;
    }
}
