<?php

declare(strict_types=1);

namespace SSE\Event;

use SSE\Contract\EventInterface;

final class Event implements EventInterface
{
    private string $id;

    private string $eventName;

    private string $data;

    private int $retry;

    private string $comment;

    public function __construct(
        string $id = '',
        string $eventName = '',
        string $data = '',
        int $retry = 5000,
        string $comment = ''
    ) {
        $this->id        = $id;
        $this->eventName = $eventName;
        $this->data      = $data;
        $this->retry     = $retry;
        $this->comment   = $comment;
    }

    public function getEventName(): string
    {
        return $this->eventName;
    }

    public function getData(): string
    {
        return $this->data;
    }

    public function __toString(): string
    {
        $event = [];
        if ('' !== $this->comment) {
            $event[] = \sprintf(': %s', $this->comment);
        }
        if ('' !== $this->id) {
            $event[] = \sprintf('id: %s', $this->id);
        }
        if ($this->retry > 0) {
            $event[] = \sprintf('retry: %s', $this->retry);
        }
        if ('' !== $this->eventName) {
            $event[] = \sprintf('event: %s', $this->eventName);
        }
        if ('' !== $this->data) {
            $event[] = \sprintf('data: %s', $this->data);
        }

        return implode("\n", $event) . "\n\n";
    }
}
