<?php declare(strict_types=1);

namespace SSE\Event;

use SSE\Contract\EventInterface;

final class PingEvent implements EventInterface
{
    private string $id;

    public function __construct(string $id = '')
    {
        $this->id = $id;
    }

    public function getEventName(): string
    {
        return 'ping';
    }

    public function getData(): string
    {
        return '';
    }

    public function __toString(): string
    {
        return implode("\n", [
                \sprintf('id: %s', $this->id),
                'retry: 5000',
                'event: ping',
            ]) . "\n\n";
    }
}
