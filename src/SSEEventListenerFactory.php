<?php declare(strict_types=1);

namespace SSE;

use SSE\Repository\EventStoreRepository;

final class SSEEventListenerFactory
{
    private EventStoreRepository $eventStoreRepository;

    public function __construct(EventStoreRepository $eventStoreRepository)
    {
        $this->eventStoreRepository = $eventStoreRepository;
    }

    /**
     * @param non-empty-list<string> $events
     */
    public function create(array $events, int $maxTimeoutWithoutResponse = 300, int $pingInterval = 15, int $maxExecutionTime = 600): SSE
    {
        $storeCollection = $this->eventStoreRepository->getCollectionEvents(...$events);
        $handler         = new SSEController($storeCollection, $maxTimeoutWithoutResponse, $pingInterval, $maxExecutionTime);

        return new SSE(fn () => $handler());
    }
}
