<?php

declare(strict_types=1);

namespace SSE\Exception;

use Exception;

class SSEStopException extends Exception
{
}
