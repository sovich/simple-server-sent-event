<?php

declare(strict_types=1);

namespace SSE;

use Closure;
use Generator;
use SSE\Contract\EventInterface;
use SSE\Exception\SSEStopException;

/**
 * @template-implements \IteratorAggregate<EventInterface>
 */
final class SSE implements \IteratorAggregate
{
    /**
     * @var Closure(): Generator<EventInterface>
     */
    private Closure $callback;

    /**
     * @param Closure(): Generator<EventInterface> $callback
     */
    public function __construct(Closure $callback)
    {
        $this->callback = $callback;
    }

    public function start(int $interval = 1000000): void
    {
        while (true) {
            try {
                foreach ($this->getIterator() as $event) {
                    echo $event;
                }
            } catch (SSEStopException $e) {
                return;
            }

            if (ob_get_level() > 0) {
                ob_flush();
            }

            flush();

            // if the connection has been closed by the client we better exit the loop
            if (1 === connection_aborted()) {
                return;
            }
            usleep($interval);
        }
    }

    /**
     * @return \Generator<EventInterface>
     */
    public function getIterator(): \Generator
    {
        return ($this->callback)();
    }
}
