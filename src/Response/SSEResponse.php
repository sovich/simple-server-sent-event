<?php

declare(strict_types=1);

namespace SSE\Response;

use SSE\SSE;
use Symfony\Component\HttpFoundation\StreamedResponse;

final class SSEResponse extends StreamedResponse
{
    /**
     * @param array<string, string> $headers
     */
    public function __construct(SSE $sse, int $interval = 1000000, int $status = 200, array $headers = [])
    {
        $headers += [
            'Content-Type'      => 'text/event-stream',
            'Cache-Control'     => 'no-cache',
            'Connection'        => 'keep-alive',
            'X-Accel-Buffering' => 'no',
        ];

        parent::__construct(static fn () => $sse->start($interval), $status, $headers);
    }
}
