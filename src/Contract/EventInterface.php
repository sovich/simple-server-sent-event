<?php declare(strict_types=1);

namespace SSE\Contract;

use Stringable;

interface EventInterface extends Stringable
{
    public function getEventName(): string;

    public function getData(): string;
}
