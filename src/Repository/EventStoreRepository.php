<?php declare(strict_types=1);

namespace SSE\Repository;

use Psr\Cache\CacheItemPoolInterface;
use SSE\Contract\EventInterface;

final class EventStoreRepository
{
    /**
     * @var array<string, EventStore>
     */
    private array $storeCollection;

    private CacheItemPoolInterface $cacheItemPool;

    public function __construct(CacheItemPoolInterface $cacheItemPool)
    {
        $this->cacheItemPool = $cacheItemPool;
    }

    public function save(EventInterface $event): self
    {
        $store = $this->getEventStore($event->getEventName());
        $store->save($event);

        return $this;
    }

    public function getEventStore(string $name): EventStore
    {
        return $this->storeCollection[$name] ?? $this->storeCollection[$name] = new EventStore($this->cacheItemPool, $name);
    }

    public function getCollectionEvents(string ...$events): CollectionEventStore
    {
        return new CollectionEventStore(array_map(fn (string $event) => $this->getEventStore($event), $events));
    }
}
