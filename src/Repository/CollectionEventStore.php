<?php declare(strict_types=1);

namespace SSE\Repository;

use Generator;
use IteratorAggregate;
use SSE\Contract\EventInterface;

/**
 * @template-implements IteratorAggregate<int, EventInterface>
 */
final class CollectionEventStore implements IteratorAggregate
{
    /**
     * @var IteratorAggregate<int, EventInterface>[]
     */
    private array $eventStoreList;

    /**
     * @param IteratorAggregate<int, EventInterface>[] $eventStoreList
     */
    public function __construct(array $eventStoreList)
    {
        $this->eventStoreList = $eventStoreList;
    }

    public function getIterator(): Generator
    {
        foreach ($this->eventStoreList as $eventStore) {
            yield from $eventStore;
        }
    }
}
