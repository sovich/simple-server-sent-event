<?php declare(strict_types=1);

namespace SSE\Repository;

use DateInterval;
use Generator;
use IteratorAggregate;
use Psr\Cache\CacheItemPoolInterface;
use SSE\Contract\EventInterface;

/**
 * @template-implements IteratorAggregate<int, EventInterface>
 */
class EventStore implements IteratorAggregate
{
    public const PREFIX = '37e90e84-e8d4-441b-9a71-c484b667be85';

    public const MAX_SAVE_PERIOD = 'PT10S';

    private CacheItemPoolInterface $cacheItemPool;

    private string $name;

    private int $cursor = 0;

    public function __construct(CacheItemPoolInterface $cacheItemPool, string $name)
    {
        $this->cacheItemPool = $cacheItemPool;
        $this->name          = $name;
    }

    public function save(EventInterface $event): self
    {
        if ($event->getEventName() !== $this->name) {
            throw new \LogicException(\sprintf('EventStore supported %s, but %s imported.', $this->name, $event->getEventName()));
        }

        $key      = self::PREFIX . $this->name;
        $item     = $this->cacheItemPool->getItem($key);
        $sequence = (int) (\is_scalar($item->get()) ? $item->get() : 0) + 1;

        $item->set($sequence);
        $item->expiresAfter(new DateInterval(self::MAX_SAVE_PERIOD));

        $eventItem = $this->cacheItemPool->getItem(\sprintf('%s-%s', $key, $sequence));
        $eventItem->expiresAfter(new DateInterval(self::MAX_SAVE_PERIOD));
        $eventItem->set(serialize($event));

        $this->cacheItemPool->save($eventItem);
        $this->cacheItemPool->save($item);

        return $this;
    }

    public function setCursor(int $cursor = null): self
    {
        $this->cursor = $cursor ?? $this->getLastCursor();

        return $this;
    }

    private function getLastCursor(): int
    {
        $key   = self::PREFIX . $this->name;
        $event = $this->cacheItemPool->getItem($key);

        if (!$event->isHit()) {
            return 0;
        }

        return (int) (\is_scalar($event->get()) ? $event->get() : 0);
    }

    public function getIterator(): Generator
    {
        $key    = self::PREFIX . $this->name;

        $lastCursor = $this->getLastCursor();

        for ($cursor = $this->cursor; $cursor < $lastCursor; ++$cursor) {
            $item = $this->cacheItemPool->getItem(\sprintf('%s-%s', $key, $cursor + 1));
            if ($item->isHit()) {
                yield $cursor + 1 => unserialize((string) (\is_scalar($item->get()) ? $item->get() : ''), ['allowed_classes' => true]);
            }
            $this->cursor = $cursor + 1;
        }

        if ($this->cursor > $lastCursor) {
            $this->cursor = 0;
        }
    }
}
